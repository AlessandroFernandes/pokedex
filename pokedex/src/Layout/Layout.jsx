import React, { Component } from 'react';
import NavMenu from './navMenu/navMenu';
import Footer from './footer/footer';

import '../assets/styles/style.css';
export default class Layout extends Component {

    render() {
        return (
            <div className="app">
                <div className="app__header">
                    <NavMenu  /> 
                </div>
                <div className="app__main">
                    { this.props.children }
                </div>
                <div className="app__footer">
                    <Footer />
                </div>
            </div>
        )
    }       
}
    