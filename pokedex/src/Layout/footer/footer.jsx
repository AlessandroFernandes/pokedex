import React from 'react';

import './footer.css';

export default function Footer() {
    return (
        <nav className="navbar__footer">
            <div className="container">
                <p className="navbar__footer___text">by. Alessandro Fernandes</p>
            </div>
        </nav>
    )
}