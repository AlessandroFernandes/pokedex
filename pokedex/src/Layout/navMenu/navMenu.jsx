import React from 'react';
import { Link } from 'react-router-dom';

import './navMenu.css';

import logo from '../../assets/img/pokemon_logo.png';

export default function NavMenu() {
    return(
        <nav className="navbar navbar-expand-lg navbar-light navbar__custom">
            <div className="container">
                <div className="container__logo">
                    <Link to="/">
                         <img  className="logo img-fluid" src={ logo } alt="logo" />
                    </Link>   
                </div>
            </div>
        </nav>
    )
}