import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import './home.css';

import logo from '../../assets/img/pokemon_logo.png';
export default class Home extends Component {
    static displayName = Home.name;

    render() {
        return (
            <div className="container__home">
                <div className="logo-main">
                    <img src={ logo } className="logo-main__img" alt="pokemon"></img>
                    <Link to="/pokedex">
                        <button className="btn btn-danger logo-main__button">Entrar</button>
                    </Link>
                </div>
            </div>
        )
    }  
}