import React, { useState, useEffect} from 'react';

import LeftPanel from './partial/leftPanel';
import RightPanel from './partial/rightPanel';
import PokemonService from '../../service/PokemonService';

import './pokedex.css';

function pickRandom(arr) {
    return arr[Math.floor(Math.random() * arr.length)];
}

export default function Pokedex(props) {

    const [ pokemonData, setPokemondata ] = useState([]);
    const [ pokemonIndex, setPokemonIndex ] = useState(1);
    const [ pokemonDescription, setPokemonDescription ]  = useState("");
    const [ speciesData, setSpeciesData ] = useState([]);
    const [ evoSprites, setEvoSprites] = useState([]);
    const [ evoNames, setEvoNames] = useState([]);
    //const [ moves, setMoves ] = useState([]);
    const [ loading, setLoading ] = useState(false);

    useEffect(()=> { changePokemon() }, [])

    const nextPokemon = () => {
        const next = Math.min(pokemonIndex + 1, 949);
        setPokemonIndex(next);
        changePokemon();
    }

    const previousPokemon = () => {
        const prev = Math.max(pokemonIndex - 1, 1);
        setPokemonIndex(prev);
        changePokemon();
    }

    const pickPokemon = n => {
        setPokemonIndex(n);
        changePokemon();
    }

    const changePokemon = () => {
        let dataResult = [];
        setLoading(true);
        PokemonService.SearchPokemon(pokemonIndex)
                      .then(data => { //setPokemonIndex(data.data.id);
                                      setPokemondata(data.data);
                                      SpeciesConsult(data.data.species.url)
                                      .then(data => { 
                                        setSpeciesData(data.data);
                                        dataResult = pickRandom(data.data.flavor_text_entries.filter(e => e.language.name === "en").map(e => e.flavor_text));
                                        setPokemonDescription(dataResult);
                                        setLoading(false);
                                        SpeciesConsult(data.data.evolution_chain.url)
                                        .then(data => { 
                                            const api = "https://pokeapi.co/api/v2/pokemon/";
                                            const first = data.data.chain;
                                            let second;
                                            let third;
                                            let evos = [];
                                            if (first) {
                                                const e1 = fetch(`${api}${first.species.name}/`);
                                                evos.push(e1);
                                                second = first.evolves_to[0];
                                            }
                                            if (second) {
                                                const e2 = fetch(`${api}${second.species.name}/`);
                                                third = second.evolves_to[0];
                                                evos.push(e2);
                                            }
                                            if (third) {
                                                const e3 = fetch(`${api}${third.species.name}/`);
                                                evos.push(e3);
                                            }
                                            Promise.all(evos)
                                                .then(responses => Promise.all(responses.map(value => value.json())))
                                                .then(dataList => {
                                                    const sprites = dataList.map(v => v.sprites.front_default);
                                                    const names = dataList.map(n => n.name);
                                                    setEvoNames(names);
                                                    setEvoSprites(sprites);
                                                });
                                        });
                                        
                            })
                      })      
    }       
            
    return (
        <div className="pokedex">
            <LeftPanel
                pData={ pokemonData }
                sData={ speciesData }
                no={ pokemonIndex - 1 }
                description={ pokemonDescription }
            />
            <Divider />
            <RightPanel
                pData={ pokemonData }
                sData={ speciesData }
                evoSprites={ evoSprites }
                evoNames={ evoNames }
                controls={{ next: nextPokemon, prev: previousPokemon, pick: pickPokemon }}
                no={ pokemonIndex }
            />
        </div>
    );
}

function Divider(props) {
    return (
        <div className="divider">
            <div className="gap" />
            <div className="hinge" />
            <div className="gap" />
            <div className="hinge" />
            <div className="gap" />
            <div className="hinge" />
            <div className="gap" />
        </div>
    );
}

function SpeciesConsult (apiV2) {
    return PokemonService.HandleConsult(apiV2);
}


