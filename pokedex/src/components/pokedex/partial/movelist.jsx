import React, { useState, useEffect } from 'react';

const  padStats = ( stat, val, sep, len) => {
    val = val || "xx";
    let output = `
    ${stat.toString()}${sep.repeat(len - (val.toString().length + stat.toString().length))}${val.toString()}`;
    return output;
}
const MovesLoading = () => {
    return (
        <div className="move-body move-screen screen">
            <div className="move-left">
                <div className="move-name" style={{ textTransform: "none" }}>
                    xxxxx xxxxx
                </div>
                <div className="move-stat">{padStats("Accuracy", "xx", ".", 16)}</div>
                <div className="move-stat">{padStats("Power", "xx", ".", 16)}</div>
                <div className="move-stat">{padStats("PP", "xx", ".", 16)}</div>
            </div>
            <div className="move-right">
                <div className="move-type">Type: xxxxx</div>
                <div className="move-learn">Learn: Lvl xx</div>
            </div>
        </div>
    );
}

const  MoveEntry = props => {
    const move = props.move;
    const name = move.name || move.names.filter(m => m.language.name === "en")[0].name;
    const acc = move.accuracy;
    const pow = move.power;
    const pp = move.pp;
    const type = move.type.name;
    const lvl = props.lvl;

    return (
        <div className="move-body move-screen screen">
            <div className="move-left">
                <div className="move-name">{name}</div>
                <div className="move-stat">{padStats("Accuracy", acc, ".", 16)}</div>
                <div className="move-stat">{padStats("Power", pow, ".", 16)}</div>
                <div className="move-stat">{padStats("PP", pp, ".", 16)}</div>
            </div>
            <div className="move-right">
                <div className="move-type">Type: {type}</div>
                <div className="move-learn">Learn: Lvl {lvl}</div>
            </div>
        </div>
    )
}
const MoveList = props => {

    const [ index, setIndex ] = useState(0);
    const [ currentMove, setCurrentMove ] = useState([]);
    const [ loading, setLoading ] = useState(false);

    useEffect(()=> { controlMove() }, [props.move])

    const controlMove = () => {
        setIndex(0);
        loadMoves();
    }

    const loadMoves = () => {
        setLoading(true);
        fetch(props.moves[index].move.url)
            .then(response => response.json())
            .then(data => { setCurrentMove(data);
                            setLoading(false);
                 });
    }

    const nextMove = () => {
        const nextIndex = Math.min(index + 1, props.moves.length - 1);
        setIndex(nextIndex);
        loadMoves();
    }

    const prevMove = () => {
        const preMove = Math.max(index - 1, 0);
        setIndex(preMove);
        loadMoves();
    }

    let moves;

    if (loading || Object.keys(currentMove).length === 0) {
        moves = <MovesLoading />;
    } else {
        const lvl = props.moves[index].version_group_details[0].level_learned_at;
        moves = <MoveEntry move={currentMove} lvl={lvl} />;
    }

    return (
        <div className="move-list">
            {moves}
            <div className="move-controls">
                <div className="move-arrow" onClick={ prevMove }>
                    <i className="fas fa-caret-up" />
                </div>
                <div className="move-arrow" onClick={ nextMove }>
                    <i className="fas fa-caret-down" />
                </div>
            </div>
        </div>
    );
}

export default MoveList;