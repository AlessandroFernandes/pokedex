import React from 'react';

import { Button, NumInput } from '../../../utils/commom';

const PokedexControls = props => {
    return (
        <div className="panel-row controls">
            <Button dir="left" onClick={props.controls.prev} />
            <NumInput no={props.no - 1} func={props.controls.pick} />
            <Button dir="right" onClick={()=> props.controls.next() } />
        </div>
    );
}

export default PokedexControls;