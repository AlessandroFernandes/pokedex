import React from 'react';

import Loading from '../../../utils/loading';
import Type from '../../../utils/type';
import PokedexControls from '../partial/pokedexControls';
import MoveList from '../partial/movelist';

const  padStats = ( stat, val, sep, len) => {
    val = val || "xx";
    let output = `
    ${stat.toString()}${sep.repeat(len - (val.toString().length + stat.toString().length))}${val.toString()}`;
    return output;
}

const ButtonChrome = () => {
    return (
        <div className="panel-row blue-buttons">
            <div className="blue-button" />
            <div className="blue-button" />
            <div className="blue-button" />
            <div className="blue-button" />
            <div className="blue-button" />
            <div className="blue-button" />
            <div className="blue-button" />
            <div className="blue-button" />
            <div className="blue-button" />
            <div className="blue-button" />
        </div>
    );
}


const PokemonEvolution = props => {
    const e1 = props.evoSprites[0];
    const e2 = props.evoSprites[1];
    const e3 = props.evoSprites[2];
    const n1 = props.evoNames[0];
    const n2 = props.evoNames[1];
    const n3 = props.evoNames[2];

    return (
        <div className="panel-row panel-evo">
            <PokemonSpriteSmall src={e1} evo="I" name={n1} />
            <PokemonSpriteSmall src={e2} evo="II" name={n2} />
            <PokemonSpriteSmall src={e3} evo="III" name={n3} />
        </div>
    );
}

const PokeBall = () => {
    return (
        <div className="pokemon-sprite pokemon-sprite-small empty-evo">
            <div className="poke-ball">
                <div className="poke-ball-top" />
                <div className="poke-ball-center">
                    <div className="poke-ball-dot" />
                </div>
                <div className="poke-ball-bottom" />
            </div>
        </div>
    );
}

const PokemonSpriteSmall = props => {
    let evoImage;

    if (props.src) {
        evoImage = <img src={props.src} alt="pokemon" className="pokemon-sprite pokemon-sprite-small" />;
    } else {
        evoImage = <PokeBall />;
    }

    return (
        <div>
            <div className="flex-center">
                <div className="evo-num">{props.evo}</div>
            </div>
            {evoImage}
            <div className="screen evo-name">{props.name || "No Data"}</div>
        </div>
    );
}

const  PokemonType = props => {
    const types = props.types;
    return (
        <div className="type-list">
            <div className="panel-header">Types</div>
            <div className="type-box">
                {types.map(t => {
                    const type = t.type.name;
                    return <Type type={type} key={type} />;
                })}
            </div>
        </div>
    );
}
const  StatLine = props => {
    return (
        <div className="stat-line">
            {padStats(props.name, props.value, ".", 20)}
        </div>
    );
}

const  PokemonStats = props => {

    const stats = props.stats;

    return (
        <div className="screen stats">
            {stats.map(s => {
                const name = s.stat.name;
                const value = s.base_stat;

                return <StatLine name={name} value={value} key={name} />;
            })}
        </div>
    );
}

const RightPanel = props => {

    const types = props.pData.types;
    const stats = props.pData.stats;
    const moves = props.pData.moves;

    if (types) {
        return (
            <div className="panel right-panel">
                <div className="panel-row">
                    <PokemonStats stats={ stats } />
                    <PokemonType types={ types } />
                </div>

                <PokemonEvolution evoSprites={props.evoSprites} evoNames={props.evoNames} />
                <ButtonChrome />
                <MoveList moves={moves} />
                <PokedexControls controls={props.controls} no={props.no} />
            </div>
        );
    } else {
        return Loading();
    }
}

export default RightPanel;
