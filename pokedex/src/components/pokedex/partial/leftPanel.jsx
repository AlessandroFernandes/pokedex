import React, { useState } from 'react';

import Loading from '../../../utils/loading';

const  PokemonName = props => {
    return (
        <div className="pokemon-name screen">
            {props.name}
            <span className="name-no">no. {props.no}</span>
        </div>
    );
}

const  PokemonSprite = props => {

    const [ front, setFront ] = useState(true);
    const [ shiny, setShiny ] = useState(false);
    const [ female, setFemale ] = useState(false);

    const buildImage = () =>  {
        const dir = front ? "front" : "back";
        const gender = female ? "_female" : "";
        const Isshiny = shiny ? "_shiny" : "_default";
        return dir + Isshiny + gender;
    }

    const toggleGender = () => {
        setFemale(!female);
        const handleFemale = () => {
            if (props.src[buildImage()]) {
                return;
            } else {
                setFemale( false );
            }
        };
        handleFemale();
    }

    const toggleShiny = () => {
        setShiny(!shiny);
        const handleShiny = props => {
            if (props.src[buildImage()]) {
                return;
            } else {
                setShiny( false );
            }
        };
        handleShiny(props);
    }

    const toggleFront = () => {
        setFront(!front);
        const handleFront = () => {
            if (props.src[ buildImage() ]) {
                return;
            } else {
                setFront( false );
            }
        };
        handleFront();
    }

    const imgSrc = props.src[ buildImage() ] || props.src.front_default;
    const funcs = { gender: toggleGender, front: toggleFront, shiny: toggleShiny };

    return (
        <div>
            <img src={imgSrc} alt="pokemon" className="pokemon-sprite" />
            <SpriteControls
                funcs={funcs}
                gender={ female }
                shiny={ shiny, props }
                front={ front }
            />
        </div>
    );
}


const  SpriteControls = props => {
    return (
        <div className="sprite-controls">
            <div
                className={"sprite-control sprite-controls-gender " + (props.gender ? "sprite-control-selected" : "")}
                onClick={props.funcs.gender}
            >
                <i className="fas fa-venus" />
            </div>
            <div
                className={"sprite-control sprite-controls-shiny " + (props.shiny ? "sprite-control-selected" : "")}
                onClick={props.funcs.shiny}
            >
                <span>shiny</span>
            </div>
            <div
                className={"sprite-control sprite-controls-rotate " + (!props.front ? "sprite-control-selected" : "")}
                onClick={props.funcs.front}
            >
                <i className="fas fa-undo" />
            </div>
        </div>
    );
}

const PokemonDescription = props => {
    return (
        <div className="pokemon-description screen">{props.description}</div>
    )
}

const  LeftPanel = props => {

    const pData = props.pData;

    if (typeof pData === "object" && Object.keys(pData).length !== 0) {
        return (
            <div className="panel left-panel">
                <PokemonName name={pData.name} no={props.no} />
                <PokemonSprite src={pData.sprites} />
                <PokemonDescription description={props.description} no={props.no} />
            </div>
        );
    } else {
        return Loading();
    }
}

export default LeftPanel;
