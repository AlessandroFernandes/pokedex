import React from 'react';
const  Type = props => {
    return (
        <div className={"type " + props.type}>{props.type}</div>
    )
}

export default Type;
