import React, { useState } from 'react';
export const NumInput = (props) => {

    const [ id, setId ]  = useState(1);
    
    const handleChange = (e) => {
        e.preventDefault();
        setId( e.target.value );
    }

    const handleClick = (e) => {
        e.preventDefault();
        props.func(id);
    }

    return (
        <div>
            <input
                type="number"
                className="screen num-input"
                placeholder={ props.no }
                onChange={ handleChange }
            />
            <div className="submit" onClick={ handleClick } />
        </div>
    );
}

export const Button = props => {
    return (
        <div className="button" onClick={props.onClick} />
    )
}