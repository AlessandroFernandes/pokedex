import axios from 'axios';

const _api = "https://pokeapi.co/api/v2/pokemon";

export default class PokemonService {

    static SearchPokemon(nPokemon) {

        return axios(`${_api}/${nPokemon}`);
    }

    static HandleConsult(api) {

        return axios(api);
    }

}