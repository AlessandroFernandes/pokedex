import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import { Layout} from './Layout';
import { Home } from './components/home';
import { Pokedex } from './components/pokedex';

export default class App extends Component {
    static displayName = App.name;

    render() {
        return (
            <Layout>
                <Switch>
                    <Route exact path="/" component={ Home } />
                    <Route path="/pokedex" component={ Pokedex } />
                    <Route path='*' component={ Home } />
                </Switch>
            </Layout>
        )
    }
}
